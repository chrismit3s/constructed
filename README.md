# Constructed - A compass and straightedge simulation

Annoyed by the inaccuracies of drawing on paper?
This tool allows you to draw on virtual paper with compass and straightedge with the precision of a computer (it's not perfect, but definitely good enough).

## Setup

Just clone this repo, create a venv, install the packages and run it.

> _Note: You need python>=3.8, as this project uses the [walrus operator](https://docs.python.org/3/whatsnew/3.8.html#assignment-expressions)._

On linux:

    sudo apt update && sudo apt install python3.8-dev libsdl-ttf2.0-dev libsdl-mixer1.2-dev libsdl-image1.2-dev  # pygame deps
    git clone https://gitlab.com/chrismit3s/constructed
    cd constructed
    python3.8 -m venv venv
    source ./venv/scripts/activate
    python -m pip install -r requirements.txt

On windows (with [py launcher](https://docs.python.org/3/using/windows.html) installed):

    git clone https://gitlab.com/chrismit3s/constructed
    cd constructed
    py -3.8 -m venv venv
    .\venv\scripts\activate.bat
    python -m pip install -r requirements.txt

Then just run the `src` module with:

    python -m src [-l] [-i FILENAME] [-P] [-L LOGLEVEL]

The `--light`/`-l` option enables the light theme, the dark theme is default.
You can import a previously exported graph with the `--import`/`-i` option.
`-P` and `-L` are for debugging purposes.

## Tools

The currently selected tool is display in the top left corner. The available tools are:

| Key | Tool       | Description |
|:---:|:-----------|:------------|
|  S  | Straight   | Select two points to draw a line through |
|  C  | Circle     | Select two points to draw a circle, first one is the center, second one determines the radius |
|  P  | Point      | Just pick any point, needed as a start |
|  I  | Intersect  | Intersect two lines, whether circle or straight doesn't matter, and put a point at the intersection |
|  H  | Hide       | Click objects to hide them to avoid clutter |
|  M  | Move       | Move points around; only works with normal points, not with intersection points |
|  D  | Delete     | Delete a object _from the canvas_, any intersections or other objects depending on it will still work normally |
|  Y  | Screenshot | Take a screenshot of the current viewport |
|  X  | Export     | Save the current graph to a file (located in ./out), can be loaded again with the `-i` option |

Moving the canvas can be done by simply holding right click and dragging.
Scrolling the mouse wheel zooms in or out.

## Objects

### Points

![a point][point-img]

A point is the most basic building block here, you always start with at least to points.
From there you can define circles, straight and intersections of them.

They can be created by the `Point` tool and moved by dragging them when the `Move` tool is selected.

### Circles

![a circle][circle-img]

Defined by two points, the first one being the center, the second one defining the radius, a circle represents every point with a certain distance to its center.

You create them with the `Circle` tool.

### Straights

![a straight][straight-img]

Straights are also defined by two points, but here it doesn't matter what order you select them in.
They represent an infinitely long line going through both points.
Mathematically speaking, they can be thought of as a circle with center at infinity.

The `Straight` tool allows you to create them.

### Intersection point

![an intersection of two circles][intersect-img]

Intersection two objects can be done with the `Intersect` tool.
An intersection represents all points with are on one object _and_ the other.
There may be zero to two points satisfying that condition, with this tool, you create objects for all of them, making them usable, selectable, hidable, but not movable.

Intersection points may be distinguished from normal points as they're smaller and filled, while normal points are twice the size and ring shaped.

## Examples

### A [regular pentagon](https://en.wikipedia.org/wiki/Pentagon#Construction_of_a_regular_pentagon).

![a regular pentagon][pentagon-img]

---

### A [regular heptadecagon (17-gon)](https://en.wikipedia.org/wiki/Heptadecagon#Construction).

![a regular heptadecagon (17-gon)][17-gon-img]

---

### A square incribed in a triangle

![a square inscribed in a triangle][square-in-triangle]

Just like it was described in [this](https://www.youtube.com/watch?v=9ptyprXFPX0) numberphile video.
To check out the interactive version, load the file `./out/square-inscribed-in-triangle.json`, or just run

    python -m src -i out\square-inscribed-in-triangle.json

after activating the venv.

## Challenges

Try and construct a [regular heptadecagon (17-gon)](https://en.wikipedia.org/wiki/Heptadecagon#Construction), like young Gauss did.

Here's how:

![instructions for construction a regular 17-gon][17-gon-instructions]

## License

This project is licensed under the MIT License - see the LICENSE.md file for details

[point-img]: ./imgs/objects/point.png
[circle-img]: ./imgs/objects/circle.png
[straight-img]: ./imgs/objects/straight.png
[intersect-img]: ./imgs/objects/intersect.png

[pentagon-img]: ./imgs/pentagon.png
[17-gon-img]: ./imgs/heptadecagon.png
[square-in-triangle]: ./imgs/square-inscribed-in-triangle.png

[17-gon-instructions]: https://upload.wikimedia.org/wikipedia/commons/e/e7/Regular_Heptadecagon_Using_Carlyle_Circle.gif
