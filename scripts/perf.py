from src import FixedPoint, Straight, Circle, intersect
from timeit import timeit
from unittest.mock import MagicMock as DummyCanvas
from random import choice, sample, shuffle
import numpy as np


# width := number of points/drawables per layer, too low and layers in which no drawables intersect and the program hangs
WIDTH = 100

# depth := number of layers
DEPTH = 50

# number of iterations to average over
N = 30000


def random_point(canvas):
    p = FixedPoint(*(np.random.uniform([-640, -360], [640, 360])))
    p.canvas = canvas
    return p

def random_drawable(*points, canvas):
    points = list(points)
    shuffle(points)

    d = choice([Straight, Circle])(*points[:2])
    d.canvas = canvas
    return d


if __name__ == "__main__":
    options = {"debug": False}
    canvas = DummyCanvas()
    canvas.options = options

    points = []
    drawables = []

    # generate some points
    print("generating starting points", end="", flush=True)
    points.append([])
    for _ in range(WIDTH):
        print(".", end="", flush=True)
        points[0].append(random_point(canvas))
    print("")

    # generate lines through points and points by intersections
    print("generating intersection points", end="", flush=True)
    while len(points) < DEPTH:
        print(".", end="", flush=True)

        # new drawables through previously generated points
        drawables.append([])
        for _ in range(WIDTH):
            drawables[-1].append(random_drawable(*sample(points[-1], 2), canvas=canvas))

        # new level of intersections
        points.append([])
        while len(points[-1]) < WIDTH:
            for p in intersect(*sample(drawables[-1], 2)):
                p.canvas = canvas
                if not np.isnan(p.pos).any():
                    points[-1].append(p)
    print("")

    p = points[-1][0]
    t = timeit(stmt="p.pos", number=N, globals=locals())
    print(f"{1000 * t / N:.1f}ms per run, {N} runs, {t:.1f}s total")
