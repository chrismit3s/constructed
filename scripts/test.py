from src import FixedPoint, Straight, Circle, intersect
from timeit import timeit
from unittest.mock import MagicMock as DummyCanvas
from random import choice, sample, shuffle
import numpy as np


def random_point(canvas):
    p = FixedPoint(*(np.random.uniform([-640, -360], [640, 360])))
    p.canvas = canvas
    return p

def random_drawable(*args, canvas):
    args = list(args[:2])
    shuffle(args)

    d = choice([Straight, Circle])(*args)
    d.canvas = canvas
    return d


if __name__ == "__main__":
    options = {"debug": False}
    canvas = DummyCanvas()
    canvas.options = options

    points = []
    drawables = []

    # generate some points
    for _ in range(10):
        points.append(random_point(canvas))

    # new drawables through previously generated points
    for _ in range(10):
        drawables.append(random_drawable(*sample(points, 2), canvas=canvas))

    # new level of intersections
    for _ in range(10):
        for p in intersect(*sample(drawables, 2)):
            p.canvas = canvas
            points.append(p)

    print(set(points))
