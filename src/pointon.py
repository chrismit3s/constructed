from functools import cached_property
from src import POINT_RADIUS
from src import Point, Straight, Circle
import logging as log
import numpy as np


def intersect(a, b, n=None):
    n = [0, 1] if n is None else [n]
    if type(a) == type(b) == Straight and not a.allclose(b):
        return [PointOnStraights(a, b)]
    elif type(a) == type(b) == Circle and not a.allclose(b):
        return [PointOnCircles(a, b, i) for i in n]
    elif type(a) == Circle:
        return [PointOnCircleAndStraight(a, b, i) for i in n]
    elif type(b) == Circle:
        return [PointOnCircleAndStraight(b, a, i) for i in n]
    else:
        log.warn(f"unknown intersection: {type(a)} and {type(b)}")
        return []


class PointOn(Point):
    def draw(self, canvas, color=None):
        # divide by scale so that points are always the same size
        r = POINT_RADIUS / canvas.scale

        # draw point
        canvas.draw_circle(self.pos, r / 2, self.get_color(color), filled=True)

    @staticmethod
    def intersect_circle_and_straight(support, direction, center, radius, n):
        diff = support - center

        # see note 2020-03-16-constructed.git
        # pluging the straight formula into the circle formula, you get a quadratic with coefficients
        a = np.dot(direction, direction)
        b = 2 * np.dot(direction, diff)
        c = np.dot(diff, diff) - radius**2

        # discriminant
        d = b**2 - 4 * a * c
        if d < 0:
            return np.array((np.nan, np.nan))

        # solve for positive or negative branch
        n = 2 * n - 1
        t = (-b + n * np.sqrt(d)) / (2 * a)

        return support + t * direction


class PointOnStraights(PointOn):
    def __init__(self, a, b):
        super().__init__()
        self.s1 = a
        self.s2 = b

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        return [self.s1, self.s2]

    @property
    @PointOn.cached
    def pos(self):
        # see note 2020-03-15-constructed.git
        # pN is the support vector, dN the direction vector of straight sN
        p1 = self.s1.support
        d1 = self.s1.direction
        p2 = self.s2.support
        d2 = self.s2.direction

        # perpendicular to d2
        dp = np.array((-d2[1], d2[0]))
        t = np.dot(dp, p2 - p1) / np.dot(dp, d1)
        return p1 + t * d1


class PointOnCircles(PointOn):
    def __init__(self, a, b, n):
        super().__init__()
        self.c1 = a
        self.c2 = b
        self.n = n  # as there are multiple solutions: "this object represents the nth one"

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        return [self.c1, self.c2]

    @property
    @PointOn.cached
    def pos(self):
        # see note 2020-03-16-constructed.git
        diff = self.c2.center - self.c1.center
        direction = np.array((-diff[1], diff[0]))  # perpendicular to vector from center1 to center2
        support = self.c1.center + diff * (self.c1.radius**2 - self.c2.radius**2 + np.linalg.norm(diff)**2) / (2 * np.linalg.norm(diff)**2)

        return PointOn.intersect_circle_and_straight(support, direction, self.c1.center, self.c1.radius, self.n)
            

class PointOnCircleAndStraight(PointOn):
    def __init__(self, c, s, n):
        super().__init__()
        self.c = c
        self.s = s
        self.n = n  # as there are multiple solutions: "this object represents the nth one"

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        return [self.c, self.s]

    @property
    @PointOn.cached
    def pos(self):
        return PointOn.intersect_circle_and_straight(self.s.support, self.s.direction, self.c.center, self.c.radius, self.n)
