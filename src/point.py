from pygame import Color
from pygame import gfxdraw as draw
from src import Drawable
from src import POINT_RADIUS
import numpy as np


class Point(Drawable):
    def __repr__(self):
        return f"{self.__class__.__name__}({self.pos[0]:.2f}, {self.pos[1]:.2f})"

    def __str__(self):
        return repr(self)

    @property
    def pos(self):
        raise NotImplemented

    def allclose(self, other):
        if not isinstance(other, Point):
            return False

        return np.allclose(self.pos, other.pos)

    def dist(self, pos):
        return np.linalg.norm(self.pos - pos)

    def on_screen(self, canvas):
        px = canvas.to_px(self.pos)
        return ((canvas.res - px + POINT_RADIUS) > 0).all()

