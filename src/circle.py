from functools import cached_property
from pygame import Color
from src import Drawable
from src import POINT_RADIUS
import numpy as np


class Circle(Drawable):
    def __init__(self, m, p):
        super().__init__()
        self.m = m
        self.p = p

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        return [self.m, self.p]

    @property
    @Drawable.cached
    def center(self):
        return self.m.pos

    @property
    @Drawable.cached
    def radius(self):
        return np.linalg.norm(self.m.pos - self.p.pos)

    def allclose(self, other):
        if not isinstance(other, Circle):
            return False

        return (np.allclose(self.center, other.center) and np.isclose(self.radius, other.radius))

    def draw(self, canvas, color=None):
        canvas.draw_circle(self.center, self.radius, self.get_color(color))

    def dist(self, pos):
        return abs(np.linalg.norm(self.center - pos) - self.radius)

    def on_screen(self, canvas):
        px = canvas.to_px(self.center)
        return ((canvas.res - px + self.radius) > 0).all()
