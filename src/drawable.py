from collections import defaultdict
from functools import cached_property, wraps, reduce
import logging as log
import numpy as np


class Drawable():
    def __init__(self):
        self.hidden = False

        # caching
        self._value_cache = {}  # caches the return values (key: method name, value: last return value)
        self._state_cache = defaultdict(lambda: {})  # caches the last access state (key: method name, value: last access state)

    def __repr__(self):
        return self.__class__.__name__ + "(" + ", ".join(f'{k}: {v!r}' for k, v in self.__dict__.items() if k not in {"hidden", "defining_fixedpoints", "defining_objects", "_value_cache", "_state_cache"}) + ")"

    def __str__(self):
        return repr(self)

    def _changed(self, method):
        """
        checks if any of the dependencies for method changed since the last access
        """
        changed = False
        for x in self.defining_fixedpoints:
            if not (x.pos == self._state_cache[method.__name__].get(x, np.nan)).all():
                self._state_cache[method.__name__][x] = x.pos
                changed = True
        if changed:
            log.info(f"defining FixedPoint changed (now {{{', '.join(str(x) for x in self.defining_fixedpoints)}}})")
        return changed

    @cached_property
    def defining_fixedpoints(self):
        """
        returns a frozenset of all FixedPoints this object depends on
        """
        return reduce(lambda acc, obj: acc | obj.defining_fixedpoints,
                      self.defining_objects,
                      frozenset())

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        raise NotImplementedError

    def get_color(self, color=None):
        # if a color was specified, use it
        if color:
            return color

        # else be hidden or not
        else:
            return "hidden" if self.hidden else "foreground"
    
    def allclose(self, other):
        raise NotImplementedError

    def draw(self, color=None):
        raise NotImplementedError

    def dist(self, pos):
        raise NotImplementedError

    def on_screen(self):
        raise NotImplementedError

    @staticmethod
    def cached(method):
        """
        caches the return value of method and only recalculates it if the objects state
        changed; more precisely: when self._cached(method) returns True
        """
        @wraps(method)
        def func(self):
            # method not called before or dependencies changed
            if method.__name__ not in self._value_cache or self._changed(method):
                self._value_cache[method.__name__] = method(self)
                log.info("cache value recalculated")
            return self._value_cache[method.__name__]
        return func
