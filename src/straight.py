from functools import cached_property
from src import Drawable
import logging as log
import numpy as np


class Straight(Drawable):
    def __init__(self, a, b):
        super().__init__()
        self.p1 = a
        self.p2 = b

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        return [self.p1, self.p2]

    @property
    @Drawable.cached
    def support(self):
        return self.p1.pos

    @property
    @Drawable.cached
    def direction(self):
        return self.p2.pos - self.p1.pos

    def allclose(self, other):
        if not isinstance(other, Straight):
            return False

        # check similar direction
        d = self.direction / other.direction
        if not np.isclose(*d):
            return False

        # check support vector
        s = (other.support - self.support) / self.direction
        if not np.isclose(*s):
            return False

        return True

    def draw(self, canvas, color=None):
        canvas.draw_line(self.p1.pos, self.direction, self.get_color(color))

    def dist(self, pos):
        # see note 2020-03-11-constructed.git
        a = self.p2.pos - self.p1.pos
        x = pos - self.p1.pos

        b = np.array((-a[1], a[0]))
        b0 = b / np.linalg.norm(b)

        return abs(np.dot(x, b0))

    def on_screen(self, canvas):
        if self.p1.on_screen(canvas) or self.p2.on_screen(canvas):
            return True

        # top left and bottom right corner of the screen in units
        tl = canvas.from_px((0, 0))
        br = canvas.from_px(canvas.res)

        s = self.support
        d = self.direction
        for p in [tl, br]:
            # check if intersection for screen border through p in x or y direction is visible
            t = (p - s) / d
            c = canvas.to_px(s + t[::-1] * d)
            if (((0, 0) <= c) & (c <= canvas.res)).any():
                return True

        return False
