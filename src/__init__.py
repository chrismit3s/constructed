from pygame import Color
from os.path import join


POINT_RADIUS = 6
MODE_TRIANGLE = 60

FONT_NAME = "Roboto Mono Medium for Powerline"
FONT_SIZE = 20

SCALE_FACTOR = 0.9
CLICK_MARGIN = 10

IMG_DIR = join(".", "imgs", "")
OUT_DIR = join(".", "out", "")

# see coolors.co/f9f1ed-ccc4c5-201d1b-60c198-3d6e90
LIGHT_PALETTE = {
    "background": Color("#F9F1ED"),
    "hidden":     Color("#CCC4C5"),
    "foreground": Color("#201D1B"),
    "selected":   Color("#60C198"),
    "preview":    Color("#3D6E90")}
# see coolors.co/0f0f10-3e3d3b-e0dfd5-f06543-b9d389
DARK_PALETTE = {
    "background": Color("#0F0F10"),
    "hidden":     Color("#3E3D3B"),
    "foreground": Color("#E0DFD5"),
    "selected":   Color("#F06543"),
    "preview":    Color("#B9D389")}


from src.drawable import Drawable
from src.point import Point
from src.fixedpoint import FixedPoint
from src.straight import Straight
from src.circle import Circle
from src.pointon import PointOn, PointOnStraights, PointOnCircles, PointOnCircleAndStraight, intersect

from src.canvas import Canvas
