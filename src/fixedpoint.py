from functools import cached_property
from src import Point
from src import POINT_RADIUS
import numpy as np


class FixedPoint(Point):
    def __init__(self, x, y):
        super().__init__()
        self._pos = np.array([x, y], dtype="float64")

    @cached_property
    def defining_fixedpoints(self):
        """
        returns a list of all drawables that directly define this object
        """
        return frozenset({self})

    @cached_property
    def defining_objects(self):
        """
        returns a list of all drawables that directly define this object
        """
        # fixed points are defined by the user
        return []

    @property
    def pos(self):
        return self._pos

    @pos.setter
    def pos(self, value):
        self._pos = np.array(value, dtype="float64")

    def draw(self, canvas, color=None):
        # divide by scale so that points are always the same size
        r = POINT_RADIUS / canvas.scale

        # draw point
        canvas.draw_circle(self.pos, r, self.get_color(color), filled=True)
        canvas.draw_circle(self.pos, r / 2, "background", filled=True)

