# todo

* Drawable.on_screen needs some fixing in some specializations
* remove unnecessary imports
* FixedPoint.__init__ takes x, y; it should take pos
* np.vectorize Canvas.to_px and Canvas.from_px
* Canvas.remove
